/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.satit.UserManagement;

import java.util.ArrayList;

/**
 *
 * @author satit
 */
public class UserService {

    private static ArrayList<User> userlist = new ArrayList<>();

    static {
        userlist.add(new User("admin", "password"));
        userlist.add(new User("user1", "password"));

    }

    public static boolean addUser(User user) {
        userlist.add(user);
        return true;
    }

    public static boolean addUser(String userName, String password) {
        userlist.add(new User(userName, password));
        return true;
    }

    public static boolean updateUser(int index, User user) {
        userlist.set(index, user);
        return true;
    }

    public static User getUser(int index) {
        if(index>userlist.size()-1){
            return null;
        }
        return userlist.get(index);
    }

    public static ArrayList<User> getUsers() {
        return userlist;
    }

    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userlist) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }

    public static boolean delUser(int index) {
        userlist.remove(index);
        return true;
    }

    public static boolean delUser(User user) {
        userlist.remove(user);
        return true;
    }

    public static User login(String userName, String password) {
        for (User user : userlist) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password));
            return user;
        }
        return null;
    }
    public static void save(){
        
    }
    public static void load(){
        
    }
}
